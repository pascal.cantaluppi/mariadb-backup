# MaraiDB Cron Backup 

## CapRover Deployment

Clone this project for a CapRover Deployment of this MaraiDB Cron-Backup Service

<p>
<img src="https://gitlab.com/pascal.cantaluppi/mariadb-backup/-/raw/main/images/logo.png" alt="CapRover" />
</p>

## Getting started

### Setup

```bash
npm i caprover -g
caprover login
```

### Deploy to CapRover

```bash
git clone https://gitlab.com/pascal.cantaluppi/mariadb-backup.git
cd mariadb-backup
caprover deploy
```

## Captain definition file

```json
{
  "schemaVersion": 2,
  "imageName": "fradelg/mysql-cron-backup"
}
```

## Captain Configuration

### HTTP Settings

<p>
<img src="https://gitlab.com/pascal.cantaluppi/vacuum/-/raw/master/img/http.png" alt="HTTP Settings" />
</p>

### Environmental Variables

<p>
<img src="https://gitlab.com/pascal.cantaluppi/mariadb-backup/-/raw/main/images/01.png" alt="Environment variables" />
</p>

### Persistent Directories

<p>
<img src="https://gitlab.com/pascal.cantaluppi/mariadb-backup/-/raw/main/images/02.png" alt="Directories" />
</p>
